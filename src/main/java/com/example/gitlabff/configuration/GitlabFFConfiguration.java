package com.example.gitlabff.configuration;

import io.getunleash.DefaultUnleash;
import io.getunleash.Unleash;
import io.getunleash.util.UnleashConfig;
import org.springdoc.core.configuration.SpringDocConfiguration;
import org.springdoc.core.properties.SpringDocConfigProperties;
import org.springdoc.core.providers.ObjectMapperProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GitlabFFConfiguration {

    @Value("${gitlab.ff.appName}")
    private String appName;
    @Value("${gitlab.ff.apiUrl}")
    private String apiUrl;
    @Value("${gitlab.ff.instanceId}")
    private String instanceId;
    @Value("${gitlab.ff.apiToken}")
    private String apiToken;
    @Value("${environment}")
    private String env;

    @Bean
    public Unleash unleashConfig() {
        UnleashConfig config = UnleashConfig.builder()
            .appName(env)
            .instanceId(instanceId)
            .unleashAPI(apiUrl)
            .environment("dev")
            //.apiKey(API_TOKEN)
            .synchronousFetchOnInitialisation(true)     // THIS TRIGGERS FeatureRepository.updateFeatures() METHOD
            .build();

        return new DefaultUnleash(config);
    }

    @Bean
    public SpringDocConfiguration springDocConfiguration() {
        return new SpringDocConfiguration();
    }

    @Bean
    public SpringDocConfigProperties springDocConfigProperties() {
        return new SpringDocConfigProperties();
    }

    @Bean
    public ObjectMapperProvider objectMapperProvider(SpringDocConfigProperties springDocConfigProperties) {
        return new ObjectMapperProvider(springDocConfigProperties);
    }

}