package com.example.gitlabff.controller;

import io.getunleash.Unleash;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GitlabFFController {

    private final Unleash unleash;

    @GetMapping("/all/all")
    public ResponseEntity<String> sayHello() {
        if (unleash.isEnabled("hello_world")) {
            return ResponseEntity.ok("Hello world!");
        } else {
            return ResponseEntity.ok("I'm not ready to say hello to the world...");
        }

    }

    @GetMapping("/hello-dev/hello-dev")
    public ResponseEntity<String> helloToDev() {
        if (unleash.isEnabled("hello_dev")) {
            return ResponseEntity.ok("Hello my developers!");
        } else {
            return ResponseEntity.ok("Developers are on holiday");
        }
    }

    @GetMapping("/hello-prod/hello-prod")
    public ResponseEntity<String> helloToProd() {
        if (unleash.isEnabled("hello_prod")) {
            return ResponseEntity.ok("Hello, I'm live!");
        } else {
            return ResponseEntity.ok("Better review something before go to live");
        }
    }

}