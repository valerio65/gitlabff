package com.example.gitlabff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabFfApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabFfApplication.class, args);
    }

}
