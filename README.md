# Getting Started

### Steps

For further reference, please consider the following sections:

* Go to gitlab, project, deploy, feature flags
* Create the feature flags needed
* Create some environments needed
* Define gitlab-ci.yml (I think it's optional)
* On feature flags page -> configure: you can get api url and instance ID
* Create a project and maven unleash-client-java in pom (it seems API TOKEN is not necessary)
* Configuration: define a configuration bean -> the app name will be the choosen environment

### Open Points

* Investigate a bit further the security - how can we block users to swap in the Gitlab UI
* Change the value of a flag from code : example e2e run, changes the FF and all services will use new value
*
    * For this point, I think the unleash library do not covers this case. Search for another library, or extend it

